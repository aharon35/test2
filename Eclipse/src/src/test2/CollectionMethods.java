//Aharon Moryoussef 
//1732787	
package src.test2;
import java.util.Collection;
import java.util.Collections;
import java.util.ArrayList;
import java.util.Arrays;

public class CollectionMethods {

	public static Collection<Planet> getLargerThan(Collection<Planet> planets, double size){
		ArrayList<Planet> larger = new ArrayList<Planet>(); 
		for(int i =0; i<planets.size(); i++) {
			if(((ArrayList<Planet>) planets).get(i).getRadius() >= size) {
				larger.add(((ArrayList<Planet>) planets).get(i));
		}
		}
		//Collections.sort(larger);  //written to test my sort method.
		//for(int i = 0; i <larger.size(); i++) {
			//System.out.println(larger.get(i).getName() + "" + larger.get(i).getRadius());
		//}
		return larger;
	}
	
	public static void main(String[] args) {
		ArrayList<Planet> larger = new ArrayList<Planet>(); 
		Planet p = new Planet("MilkyWay","Earth",1, 17.0);
		Planet a = new Planet("MilkyWay","Earth",1, 15.0);
		Planet b = new Planet("MilkyWay","Mercury",1, 18);
		Planet c = new Planet("MilkyWay","Mercury",1, 20);
		larger.add(p);
		larger.add(a);
		larger.add(b);
		larger.add(c);
	     
		CollectionMethods q  = new CollectionMethods();
		
		q.getLargerThan(larger, 15); // to test my getLargerThan method.
		if(larger.get(0).equals(larger.get(1))) {
			System.out.println("true");   //testing equals method
		}else {
			System.out.println("false");
		}
		

	}
	

}
