//Aharon Moryoussef
//1732787
public class HourlyEmployee implements Employee {
	private double hourlyPay;
	private double hoursPerWeek;
	
	public HourlyEmployee(double hourlyPay,double hoursPerWeek) {
		this.hourlyPay = hourlyPay;
		this.hoursPerWeek = hoursPerWeek;
	}
	
	public double getHoursPerWeek() {
		return this.hoursPerWeek;
	}
	public double getHourlyPay() {
		return this.hourlyPay;
	}
	
	public double getWeeklyPay() {
	   double weeklyPay = this.hourlyPay * this.hoursPerWeek;
		return weeklyPay;
	}

}
