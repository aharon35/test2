//Aharon Moryoussef
//1732787
package src.test2;
import java.util.Arrays;
/**
 * A class to store information about a planet
 * @author Daniel
 *
 */
public class Planet implements Comparable<Planet> {
	/**
	 * The name of the solar system the planet is contained within
	 */
	private String planetarySystemName;
	
	/**
	 * The name of the planet.
	 */
	private String name;
	
	/**
	 * From inside to outside, what order is the planet. (e.g. Mercury = 1, Venus = 2, etc)
	 */
	private int order;
	
	/**
	 * The size of the radius in Kilometers.
	 */
	private double radius;

	/**
	 * A constructor that initializes the Planet object
	 * @param planetarySystemName The name of the system that the planet is a part of
	 * @param name The name of the planet
	 * @param order The order from inside to out of how close to the center the planet is
	 * @param radius The radius of the planet in kilometers
	 */
	public Planet(String planetarySystemName, String name, int order, double radius) {
		this.planetarySystemName = planetarySystemName;
		this.name = name;
		this.order = order;
		this.radius = radius;
	}
	
	/**
	 * @return The name of the planetary system (e.g. "the solar system")
	 */
	public String getPlanetarySystemName() {
		return planetarySystemName;
	}
	
	/**
	 * @return The name of the planet (e.g. Earth or Venus)
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return The rank of the planetary system
	 */
	public int getOrder() {
		return order;
	}


	/**
	 * @return The radius of the planet in question.
	 */
	public double getRadius() {
		return radius;
	}
	//2 planets are considered equal if their name and the planetarySystemName
	//are identical,therefore i rewrote the equals method to match that 
	public boolean equals(Object obj) {
		Planet cast = (Planet) obj;
		if((this.name != cast.name) || this.getPlanetarySystemName() != cast.getPlanetarySystemName()) {
			return false;
		}else {
			return true;
		}
	}
	//Overiding the hascode method to
	public int hashCode() {
		String combined = this.name + this.getName();
		return combined.hashCode();
	}
	//This indicates to the sort method how to compare planets and how they should be sorted
	//it sorts by name first, if they are the same, it will sort by radius(decreasing order)
	public int compareTo(Planet p) {
		int nameCompare = this.getName().compareTo(p.getName());
		if(nameCompare == 0) {
			if(this.getRadius() < p.getRadius()) {
				return 1;
			}
			
			if(this.getRadius() > p.getRadius()) {
				return -1;
			}
			
		}
		
		return nameCompare;
	}
	
	
}
