//Aharon Moryoussef
//1732787
public class SalariedEmployee implements Employee {
	private double yearlySalary;
	
	public SalariedEmployee(double yearlySalary) {
		this.yearlySalary = yearlySalary;
	}
	public double getYearlySalary() {
		return this.yearlySalary;
	}
	
	
	public double getWeeklyPay() {
		double weeklyPay =  this.yearlySalary/52;
		return weeklyPay;
	}

}
