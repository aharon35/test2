//Aharon Moryoussef
//1732787
public class UnionizedHourlyEmployee extends HourlyEmployee implements Employee {
	private double maxHoursPerWeek;
	private double overtimeRate;
	
	public double getMaxHoursPerWeek() {
		return this.maxHoursPerWeek;
	}
	public double getOvertimeRate() {
		return this.overtimeRate;
	}
	//inherits from the hourlyEmployee class
	public UnionizedHourlyEmployee(double hourlyPay, double hoursPerWeek,double maxHoursPerWeek,double overtimeRate) {
		super(hourlyPay, hoursPerWeek);
		this.maxHoursPerWeek = maxHoursPerWeek;
		this.overtimeRate = overtimeRate;
	
		// TODO Auto-generated constructor stub
	}

	@Override
	//needs to be written because Employee interface implements it
	public double getWeeklyPay() {
		double hoursPerWeek = this.getHoursPerWeek();
		double maxHours = this.getMaxHoursPerWeek();
		double overtimeHours = hoursPerWeek - maxHours;
		double weeklyPay;
		if(hoursPerWeek<=maxHours) {
			weeklyPay = hoursPerWeek * this.getHourlyPay(); //This is when the employee does not do overtime
		}else {
			//this is when the employee has overtime hours. for overtime, he gets an overtime rate and that rate is only applied to the extra hours 
			weeklyPay =  (maxHours * this.getHourlyPay()) + (this.getOvertimeRate()*this.getHourlyPay() * overtimeHours); 
		}
		
		return weeklyPay;
	}

}
