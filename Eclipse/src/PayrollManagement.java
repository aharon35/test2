//Aharon Moryoussef
//1732787
public class PayrollManagement {

	public static void main(String[] args) {
		Employee[] array = new Employee [5];
		   array[0] = new SalariedEmployee(52000.0); //1000
		   array[1] = new HourlyEmployee(10.0,24.0); //240
		   array[2] = new UnionizedHourlyEmployee(10.0,45.0,30.0,1.5);//525
		   array[3] = new SalariedEmployee(100000.0);//1923.076
		   array[4] = new HourlyEmployee(13.0,20.0);//260
		   PayrollManagement p  = new PayrollManagement();
		   System.out.println("Total expenses: " + p.getTotalExpenses(array));

	}
	//This calculates the amount being payed across all employees
	//Rounded the weekly expenses because it is money
	public double getTotalExpenses(Employee[] array) {
		double weeklyExpenses = 0;
		
		for(int i = 0; i < array.length; i++) {
			weeklyExpenses = weeklyExpenses + array[i].getWeeklyPay();
		}
		return Math.round(weeklyExpenses);
	}

}
